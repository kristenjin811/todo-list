const Api = (() => {
  const url = 'https://jsonplaceholder.typicode.com/todos'
  const getData = fetch(url).then((response) => response.json())
  return {
    getData,  // a promise object
  }
})()

const View = (() => {
  let domSelector = {
    container: ".todo-container",
    inputBox: "#user-input",
    btn: "#addBtn",
  }

  const createTmp = (arr) => {
    let template = ""
    arr.forEach((todo) => {
      template += `<li>
        <span>${todo.id}</span>
        <span>${todo.title}</span>
        <button id="${todo.id}" class="delete-btn">Delete</button>
      </li>`
    })
    return template
  }

  const render = (ele, template) => {
    ele.innerHTML = template
  }

  return {
    domSelector,
    createTmp,
    render
  }

})()

const Model = ((api, view) => {
  const { domSelector, createTmp, render } = view
  const { getData } = api
  class State {
    constructor() {
      this._todoList = [];
    }

    get getTodoList(){
      return this._todoList
    }

    set newTodoList(newList) {
      this._todoList = newList
      let todoContainer = document.querySelector(domSelector.container)
      let tmp = createTmp(this._todoList)
      render(todoContainer, tmp)
    }
  }

  return {
    State,
    getData
  }
})(Api, View)


const Controller = ((view, model) => {
  const { domSelector } = view
  const { State, getData } = model

  const state =  new State() // calling the method from the constructor in the State class

  const init = () => {
    getData.then((data) => {
      state.newTodoList = data // pass in global data into the state
    })
  }

  const addTodo = () => {
    const userInput = document.querySelector(domSelector.inputBox)
    const btn = document.querySelector(domSelector.btn)

    btn.addEventListener('click', () => {
      let item = {
        title: userInput.value,
        id: Math.floor(Math.random()*100) + 200 // (0-1) *100 => 0 - 99 => 0 - 299
      }
      const newList = [item, ...state.getTodoList]
      state.newTodoList = newList
      console.log(newList)
      userInput.value = ""
    })
  }

  const deleteTodo = () => {
    const todoContainer = document.querySelector(domSelector.container)

    todoContainer.addEventListener('click', (e) => {
      // e.target --> the HTML element that triggered the event
      // e.target.classList returns a 'DOMTokenList' object that represents the list of classes assigned to the clicked element
      console.log("e::: ", e)
      console.log("e.target::: ", e.target)
      console.log("e.target.classList::: ", e.target.classList)
      console.log("e.target.id::: ", parseInt(e.target.id))

      if (e.target.classList.contains('delete-btn')) {
        const todoId = parseInt(e.target.id)
        const newList = state.getTodoList.filter(todo => todo.id !== todoId)
        state.newTodoList = newList
      }
    })
  }
  // the function listens for a click event on the todoContainer, and then check
    // if the clicked element has the class 'delete-btn', if it does, it retreives
    // the id of the corresponding todo item, filters the getTodoList array to
    // exclude that item, and updates the newTodoList property of the state object
    // with the new array

  // wrap all functions
  const bootstrap = () => {
    init()
    addTodo()
    deleteTodo()
  }

  return {
    bootstrap,
  }
})(View, Model, Api)

Controller.bootstrap()
